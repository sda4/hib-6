package sda.jpa;

import sda.jpa.model.Klient;
import sda.jpa.model.KlientNiepelnoletni;
import sda.jpa.model.Plec;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Example0 {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            // TODO: zmienic strategie dziedziczenia na TABLE_PER_CLASS

            Klient klientRodzic = em.find(Klient.class, 1L);

            KlientNiepelnoletni kn = new KlientNiepelnoletni();
            kn.setKlientId(123L);
            kn.setRodzic(klientRodzic);
            kn.setPlec(Plec.K);
            kn.setImie("Dorota");
            kn.setNazwisko("Nowak");
            kn.setUlubionyKolor("niebieski");

            em.getTransaction().begin();
            em.persist(kn);
            em.getTransaction().commit();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
