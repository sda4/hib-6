package sda.jpa;

import sda.jpa.model.AutorKsiazki;
import sda.jpa.model.Ksiazka;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.InputStream;
import java.util.Scanner;

public class Example2 {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            // TODO:
            // 1. plik dane.csv zawiera dane ksiazek, ktore trzeba wczytac do naszej biblioteki
            // 2. kazdy wiersz w pliku dotyczy pojedynczej ksiazki
            // 3. podane sa dane nastepujacych kolumn: rodzaj, autor_imie, autor_nazwisko, tytul, min_wiek
            // 4. wartosci kolumn sa oddzielone znakiem '|'
            // 5. prosze wczytac do bazy, korzystajac z JPA, wszystkie ksiazki z pliku
            // 6. zakladamy, ze dane w pliku sa poprawne (nie brakuje kolumn, wiek jest liczba)
            em.getTransaction().begin();
            InputStream is = Example2.class.getResourceAsStream("/dane-sample.csv");
            Scanner scanner = new Scanner(is);
            while (scanner.hasNextLine()) {
                String linia = scanner.nextLine();
                String[] wartosci = linia.split("\\|");

                Ksiazka ksiazka = Ksiazka.builder()
                        .rodzajKsiazki(wartosci[0])
                        .autor(AutorKsiazki.builder()
                                .imieAutora(wartosci[1])
                                .nazwiskoAutora(wartosci[2])
                                .build())
                        .tytul(wartosci[3])
                        .wiekMinimalnyCzytelnika(Integer.valueOf(wartosci[4]))
                        .build();

                em.persist(ksiazka);
            }
            em.getTransaction().commit();
            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
