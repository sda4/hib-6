package sda.jpa.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Data
@Embeddable
public class OcenaKey implements Serializable {

    @ManyToOne
    @JoinColumn(name="ksiazka_id")
    private Ksiazka ocenianaKsiazka;

    @ManyToOne
    @JoinColumn(name="klient_id")
    private Klient oceniajacy;
}
