package sda.jpa.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Recenzja {
    @Id
    @Column(name="recenzja_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRecenzji;

    @Column(name="recenzja_tekst")
    private String trescRecenzji;

    @ManyToOne
    @JoinColumn(name="ksiazka_id")
    private Ksiazka ksiazka;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "klient_id")
    private Klient klient;
}
