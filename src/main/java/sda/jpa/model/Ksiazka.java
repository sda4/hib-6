package sda.jpa.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Ksiazka {
    @Id
    @Column(name = "ksiazka_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idKsiazki;

    private String tytul;

    @Embedded
   private AutorKsiazki autor;

    @Column(name="min_wiek")
    private Integer wiekMinimalnyCzytelnika;

    @Column(name = "rodzaj")
    private String rodzajKsiazki;

    @OneToMany(mappedBy = "idOceny.ocenianaKsiazka", fetch = FetchType.LAZY)
    private List<OcenaKsiazki> ocenyKsiazki;

    @ManyToMany
    @JoinTable(name="ksiazka_gatunek",
            joinColumns = {@JoinColumn(name="id_ksiazki")},
            inverseJoinColumns = {@JoinColumn(name="id_gatunku")})
    private List<GatunekKsiazki> gatunki;

    @OneToMany(mappedBy = "ksiazka")
    private List<Recenzja> recenzje;

    @Column(name = "liczba_wypozyczen")
    private Integer liczbaWypozyczen;
}
