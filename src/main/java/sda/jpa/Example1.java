package sda.jpa;

import sda.jpa.model.*;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

public class Example1 {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            // TODO:
            // 1. dodac do tabeli ksiazka kolumne liczba_wypozyczen (int)
            // 2. zmapowac kolumne
            // 3. wszystkim ksiazkom w bazie nadac w tej kolumnie wartosc 0 (uzywajac Hibernate)
            // 4. usunac wszystkie oceny gorsze niz 5
            em.getTransaction().begin();
            Query q = em.createQuery("Update Ksiazka k set k.liczbaWypozyczen=:liczbaWypo");
            q.setParameter("liczbaWypo", 0);
            int liczba = q.executeUpdate();
            System.out.println(liczba);
            em.getTransaction().begin();

            Query q2 = em.createQuery("DELETE from OcenaKsiazki o where o.ocena<:paramOcena");
            q2.setParameter("paramOcena", 5);
            int liczba2 = q2.executeUpdate();
            System.out.println(liczba2);
            em.getTransaction().commit();

            em.getTransaction().commit();
            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
