package sda.jpa.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "ocena_ksiazki")
public class OcenaKsiazki {

    @EmbeddedId
    private OcenaKey idOceny;

    private Integer ocena;
}
